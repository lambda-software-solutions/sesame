(defproject sesame "0.1.0-SNAPSHOT"
  :description "OAuth 2.0 / OpenID Connect identity server"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  
  :source-paths ["src/clj", "src/cljc"]
  :repl-options {:init-ns user}

  :main sesame.core
  :uberjar-name "sesame-standalone.jar"

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.229"]
                 [com.stuartsierra/component "0.3.1"]
                 [reloaded.repl "0.2.3"]
                 [aleph "0.4.3"]
                 [yada "1.2.6" :exclusions [aleph]]
                 [aero "1.0.1"]
                 [buddy/buddy-sign "1.2.0"]
                 [buddy/buddy-hashers "1.0.0"]
                 [org.clojure/java.jdbc "0.6.2-alpha3"]
                 [org.postgresql/postgresql "9.4.1211"]
                 [com.jolbox/bonecp "0.8.0.RELEASE"]
                 [ragtime "0.6.3"]
                 [com.grammarly/perseverance "0.1.0"]
                 [clj-time "0.12.2"]
                 [com.taoensso/nippy "2.12.2"]
                 [com.taoensso/timbre "4.7.4"]
                 [com.taoensso/carmine "2.15.0"]
                 [selmer "1.10.0"]
                 [danlentz/clj-uuid "0.1.7"]]

  :plugins [[lein-figwheel "0.5.10"]]
  
  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["/src/cljs" "src/cljc"]
                :figwheel {:on-jsload "sesame.core/on-js-reload"
                           :open-urls ["http://localhost:3000/dashboard"]}
                :compiler {:main sesame.core
                           :asset-path "js/compiled/out"
                           :output-to "resources/public/js/compiled/sesame.js"
                           :output-dir "resources/public/js/compiled/out"
                           :source-map-timestamp true
                           :preloads [devtools.preload]}}
               {:id "min"
                :source-paths ["/src/cljs" "src/cljc"]
                :compiler {:output-to "resources/public/js/compiled/sesame.js"
                           :main sesame.core
                           :optimizations :advanced
                           :pretty-print false}}]}

  :figwheel {:init     user/start-server
             :destroy  user/stop-server
             :css-dirs ["resources/public/css"]}

  :aliases {"package" ["do" "clean"
                       ["cljsbuild" "once" "min"]
                       ["ring" "uberjar"]]
            "build" ["do" ["clean"] ["compile"] ["uberjar"]]}
  :jvm-opts ["-Xms512m" "-Xmx1g"]

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.0"]
                                  [figwheel-sidecar "0.5.10-SNAPSHOT"]
                                  [com.cemerick/piggieback "0.2.1"]]
                   :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]
                   :source-paths ["src/clj" "src/cljs" "dev"]}
             :uberjar {:aot [sesame.core]}})
