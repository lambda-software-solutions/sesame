We are renaming "client" to "application" within both the application code and
Database.

We are aware that "client" is the OAuth 2.0 term for an application that would
like to access a Resource owned by the Resource Owner. However, we would prefer
to use common and unambiguous terminology internally, and since the idea of a
"client" and a "tenant" are somewhat similar under common usage, we choose the
more general term, "application". This is consistent with our choice of the
term "user" as opposed to "resource_owner".

We will maintain valid OAuth 2.0 endpoints in which we use "client_id" to refer
to an application.
