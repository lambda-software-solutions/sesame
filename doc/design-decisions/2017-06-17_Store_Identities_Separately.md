We are creating an "identity" database to store a normalized version of user
identities.

Instead of keeping a password_digest in the user table, we are merging the
concept of a user's password to log in to Sesame and their identity in an
external identity provider. The identity table contains a user id, some sort of
token that represents the user in some system, and the token type. For Sesame
passwords, we will store a hashed version of the password that the user used
when they signed up with a type of "password_digest".

There may be cases in which a user is created with an external identifier only.
This may be the case when they sign up with an invite code and opt to
authenticate with an external provider only.
