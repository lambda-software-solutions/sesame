FROM clojure:lein-alpine

EXPOSE 3000

RUN mkdir -p /srv
ADD target/sesame-standalone.jar /srv/sesame.jar

CMD ["java", "-jar", "/srv/sesame.jar"]
