build: ui server docker

ui: ui-clean ui-build

ui-clean:
	rm -rf ui/node_modules
	rm -rf ui/.sass-cache
	rm -rf resources/public

ui-build:
	docker run --rm --name=sesame-js-build \
	-v "$$(pwd)/ui:/tmp" \
	node:8 \
	sh -c 'cd /tmp && npm i && npm run build'
	mkdir -p resources/public
	cp -R ui/dist/* resources/public/

server:
	docker run --rm --name=sesame-clj-build \
	-v "$$(pwd):/tmp" \
	--env _JAVA_OPTIONS="-Xms512m -Xmx1g" \
	--env JAVA_TOOL_OPTIONS="-Xms512m -Xmx1g" \
	clojure:lein-alpine \
	sh -c 'cd /tmp && lein build'

docker:
	docker build -t sesame .
	docker tag sesame sesame:latest
