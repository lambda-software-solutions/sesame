(ns sesame.core
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [aleph.netty :refer [wait-for-close]]
            [ragtime.jdbc :as rjdbc]
            [ragtime.repl :as ragtime]
            [perseverance.core :as perseverance]
            [sesame.system :refer  [system]])
  (:import [java.sql SQLException]))


(defn migrate-db [sys]
  (ragtime/migrate {:datastore (rjdbc/sql-database (get-in sys [:db :conn]))
                    :migrations (rjdbc/load-resources "migrations")}))


(defn retriable-migrate-db
  [sys]
  (perseverance/retriable
   {:catch [SQLException]
    :tag ::connection}
   (migrate-db sys)))


(defn -main [& args]
  (let [sys (component/start (system))]
    ;; Attempt to apply migrations to the database with a progressive backoff
    ;; on failure (likely due to database not being ready)
    (perseverance/retry {:strategy (perseverance/progressive-retry-strategy
                                    :initial-delay 1000
                                    :stable-length 2
                                    :multiplier 2
                                    :max-delay 30000)}
                        (retriable-migrate-db sys))
    ;; Block the main thread on the web server to prevent the process
    ;; from getting killed
    (wait-for-close (get-in sys [:web :server :server]))))
