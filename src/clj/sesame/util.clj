(ns sesame.util)

(defn first-non-nil [xs]
  (some some? xs))

(defn unix-time [dt]
  (long (/ (.getMillis dt) 1000)))
