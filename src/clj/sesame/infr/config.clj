(ns sesame.infr.config
  (:require [aero.core :as aero]
            [clojure.java.io :as io]))

(defn config [profile]
  (aero/read-config (io/resource "config.edn")
                    {:profile profile}))

(def web :web)
(def db :db)
(def redis :redis)
(def jwt :jwt)
