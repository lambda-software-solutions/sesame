(ns sesame.infr.redis
  (:require [com.stuartsierra.component :as component]
            [taoensso.carmine :as car :refer [wcar]]
            [taoensso.timbre :as timbre :refer [info]]))

(defrecord ConnectionPool [config]
  component/Lifecycle
  (start [component]
    (let [conn {:pool {} :spec config}]
      (info "Starting Redis connection pool")
      (wcar conn (car/ping))
      (assoc component :conn conn)))
  (stop [component]
    (info "Stopping Redis connection pool")
    (assoc component :conn nil)))

(defn new-connection-pool [config]
  (->ConnectionPool config))
