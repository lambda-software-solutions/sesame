(ns sesame.infr.jwt
  (:require [com.stuartsierra.component :as component]
            [buddy.core.keys :as keys]
            [buddy.sign.jwt :as jwt]
            [taoensso.timbre :as timbre :refer [info error]]))

(defprotocol iJwtSigner
  (sign [this claims])
  (unsign [this claims]))

(defrecord JwtSigner [config private-key public-key]
  component/Lifecycle
  (start [component]
    (info "Starting JWT message signer")
    (assoc component
           :private-key (keys/str->private-key (:private-key config))
           :public-key (keys/str->public-key (:public-key config))))
  (stop [component]
    (info "Stopping JWT message signer")
    (assoc component
           :private-key nil
           :public-key nil))

  iJwtSigner
  (sign [this claims]
    (jwt/sign claims private-key {:alg :rs256}))
  (unsign [this claims]
    (jwt/unsign claims public-key {:alg :rs256})))

(defn new-jwt-signer [config]
  (map->JwtSigner {:config config}))
