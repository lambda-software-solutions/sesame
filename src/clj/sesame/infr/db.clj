(ns sesame.infr.db
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as timbre :refer [info error]]
            [clojure.java.jdbc :as jdbc])
  (:import [com.jolbox.bonecp BoneCPDataSource]))

(extend-protocol clojure.java.jdbc/ISQLParameter
  clojure.lang.IPersistentVector
  (set-parameter [v ^java.sql.PreparedStatement stmt ^long i]
    (let [conn (.getConnection stmt)
          meta (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v)))))

(extend-protocol clojure.java.jdbc/IResultSetReadColumn
  java.sql.Array
  (result-set-read-column [val _ _]
    (into [] (.getArray val))))

(defrecord ConnectionPool [config]
  component/Lifecycle
  (start [component]
    (info "Starting database connection pool")
    (let [{:keys [host port user password database
                  init-pool-size max-pool-size idle-time partitions]} config
          jdbc-url (format "jdbc:postgresql://%s:%d/%s" host port database)
          min-connections (inc (quot init-pool-size partitions))
          max-connections (inc (quot max-pool-size partitions))
          pooled-conn (doto (BoneCPDataSource.)
                        (.setDriverClass "org.postgresql.Driver")
                        (.setJdbcUrl jdbc-url)
                        (.setUsername user)
                        (.setPassword password)
                        (.setMinConnectionsPerPartition min-connections)
                        (.setMaxConnectionsPerPartition max-connections)
                        (.setPartitionCount partitions)
                        (.setStatisticsEnabled true)
                        (.setIdleMaxAgeInMinutes idle-time))
          jdbc-conn {:datasource pooled-conn}]
      (assoc component :conn jdbc-conn)))
  (stop [component]
    (info "Stopping database connection pool")
    (try
      (.close (get-in component [:conn :datasource]))
      (catch Exception e
        (error e "Error closing db connection pool")))
    (dissoc component :conn)))

(defn new-connection-pool [config]
  (->ConnectionPool config))
