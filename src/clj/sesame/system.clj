(ns sesame.system
  (:require [com.stuartsierra.component :as component]
            [sesame.infr.config :as config]
            [sesame.infr.db :as db]
            [sesame.infr.jwt :as jwt]
            [sesame.infr.redis :as redis]
            [sesame.www.core :as www]))

(defn profile []
  {:post [(contains? #{:dev :prod :test} %)]}
  (-> (System/getenv "APP_ENV")
      (or "dev")
      (.toLowerCase)
      keyword))

(defn system []
  (let [cfg (config/config (profile))]
    (component/system-map
     :db (db/new-connection-pool (config/db cfg))
     :redis (redis/new-connection-pool (config/redis cfg))
     :jwt (jwt/new-jwt-signer (config/jwt cfg))
     :web (component/using
           (www/new-web-server (config/web cfg))
           [:db :redis :jwt]))))
