(ns sesame.www.api
  (:require [yada.yada :as yada]
            [yada.handler :as handler]
            [yada.interceptors :as interceptors]
            [taoensso.timbre :as timbre :refer [info error]]
            [sesame.domain.oauth2 :as oauth2]
            [sesame.infr.jwt :as jwt]
            [sesame.util :as util]
            [sesame.www.util :refer [default-resource]])
  (:import [java.sql SQLException]))


(def supported-representations
  [{:media-type "application/json"}
   {:media-type "application/edn"}
   {:media-type "application/transit+json"}])


(defn clean-resource [r whitelist blacklist]
  (if whitelist
    (select-keys r whitelist)
    (apply dissoc r blacklist)))


(defn filter-response-body
  "Interceptor to filter entities in response body according to the
  :sesame.properties/whitelist and :sesame.properties/blacklist on the resource."
  [ctx]
  (let [{whitelist :sesame.properties/whitelist
         blacklist :sesame.properties/blacklist} (:resource ctx)
        {:keys [body]} (:response ctx)]
    (assoc-in ctx [:response :body]
              (if (sequential? body)
                (map #(clean-resource % whitelist blacklist) body)
                (clean-resource body whitelist blacklist)))))


(defn sql-error [e]
  (let [msg (.getMessage e)
        sql-state (.getSQLState e)
        err-class (.substring sql-state 0 2)]
    (case err-class
      "01" [(format "Warning: %s" msg) 500]
      "02" ["No data" 404]
      "03" ["Statement not complete" 500]
      "08" ["Error connecting to database" 500]
      "22" [(format "Data error: %s" msg) 500]
      "23" (case sql-state
             "23000" ["Constraint failed" 409]
             "23001" ["Restrict violation" 409]
             "23502" ["Unexpected null data" 400]
             "23503" ["Related entity does not exist" 409]
             "23505" ["Entity not unique" 409]
             "23514" ["Check failed" 409]
             "23P01" ["Contains data that should be excluded" 400]
             ["Integrity constraint violation" 409])

      (do (error e "Unknown database error")
          ["Unknown database error" 500]))))


(defmulti dispatch-error
  (fn [e ctx] (type e)))

(defmethod dispatch-error org.postgresql.util.PSQLException
  [e ctx]
  (let [[message status] (sql-error e)]
    (update-in ctx [:response]
               merge {:status status
                      :body {:message message}})))

(defmethod dispatch-error clojure.lang.ExceptionInfo
  [e ctx]
  (let [data (ex-data e)]
    (update-in ctx [:response]
               merge {:status (get data :status 500)
                      :body {:error (get data :error {})
                             :message (.getMessage e)}})))

(defmethod dispatch-error :default
  [e ctx]
  (error e "Unhandled error")
  (update-in ctx [:response]
             merge {:status (get (ex-data e) :status 500)
                    :body {:error {}
                           :message "Unknown server error"}}))


(defn api-error [ctx]
  (dispatch-error (:error ctx) ctx))


(defn prepend-interceptor [interceptors target interceptor]
  (mapcat (fn [i]
            (if (= target i)
              [interceptor i]
              [i]))
          interceptors))

(defn resource [jwt overrides]
  (-> {:consumes supported-representations
       :produces supported-representations
       :interceptor-chain yada/default-interceptor-chain
       :error-interceptor-chain yada/default-error-interceptor-chain}
      (handler/append-interceptor
       interceptors/invoke-method
       filter-response-body)
      (update-in [:error-interceptor-chain]
                 prepend-interceptor interceptors/create-response api-error)
      (merge (default-resource jwt))
      (merge overrides)))
