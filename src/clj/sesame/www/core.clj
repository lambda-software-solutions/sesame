(ns sesame.www.core
  (:require [clojure.java.io :as io]
            [com.stuartsierra.component :as component]
            [yada.yada :as yada]
            [yada.resources.classpath-resource :as r-classpath]
            [yada.resources.webjar-resource :as r-webjar]
            [bidi.bidi :as bidi]
            [bidi.vhosts :as vhosts]
            [taoensso.timbre :as timbre :refer [info error]]
            [cheshire.generate :as json-gen]
            [sesame.domain.auth] ; required for side effect of registering auth multimethods
            [sesame.www.oauth2 :as oauth2]
            [sesame.www.applications :as applications]
            [sesame.www.tenants :as tenants]
            [sesame.www.users :as users]
            [sesame.www.login :as login])
  (:import [java.net InetSocketAddress Inet4Address]))

(json-gen/add-encoder java.util.UUID
                      (fn [u jsonGenerator]
                        (.writeString jsonGenerator (str u))))

(defn api-routes [config db redis jwt]
  [""
   [
    (tenants/routes config db redis jwt)
    (users/routes config db redis jwt)
    (applications/routes config db redis jwt)]])


(defn routes [config db redis jwt]
  [""
   [
    (oauth2/routes config db redis jwt)
    (login/routes config db redis jwt)

    ["/api/v1" (-> (api-routes config db redis jwt)
                   (yada/swaggered
                    {:info {:title "Sesame API"
                            :version "1.0"
                            :description "API endpoints for Sesame auth server"}
                     :basePath "/api/v1"})
                   (bidi/tag :sesame.resources/api))]

    ["/swagger" (-> (r-webjar/new-webjar-resource "/swagger-ui" {:index-files ["index.html"]})
                    (bidi/tag :sesame.resources/swagger))]

    ["/assets" (-> (r-classpath/new-classpath-resource "public"))]

    ;; Fall back - 404
    [true (yada/handler nil)]]])

(defn hostname [{:keys [host port]}]
  (if (and port (= 80 port))
    host
    (format "%s:%d" host port)))

(defrecord WebServer [config server db redis jwt]
  component/Lifecycle
  (start [component]
    (let [{:keys [host port protocol]} config]
      (info (format "Starting web handler on %s:%d" host port))
      (let [handler (vhosts/vhosts-model [{:scheme (keyword protocol) :host (hostname config)}
                                          (routes config (:conn db) (:conn redis) jwt)])
            server (yada/listener handler {:port port})]
        (assoc component :server server))))
  (stop [component]
    (when server
      (info "Stopping web handler")
      ((:close server))
      (assoc component :server nil))))

(defn new-web-server [config]
  (map->WebServer {:config config}))
