(ns sesame.www.login
  (:require [yada.yada :as yada]
            [schema.core :as s]
            [clj-time.core :as time]
            [manifold.deferred :as d]
            [selmer.parser :as s.parser]
            [sesame.infr.jwt :as jwt]
            [sesame.util :as util]
            [sesame.domain.users :as users]
            [sesame.domain.tenants :as tenants]))

(def LoginForm
  {:email s/Str
   :password s/Str})

(defn render-login [config ctx]
  (s.parser/render-file "views/login/login.html"
                        {:config config}))

(defn redir-to [ctx path]
  (assoc (:response ctx)
         :status 302
         :headers {"location" path}))


(defn abridged-tenant [t]
  (select-keys t [:id :name :type :roles]))


(defn handle-login [config db jwt ctx]
  ;; If login validates, set cookie and redirect to:
  ;;  - If user belongs to any managing organization, "/dashboard"
  ;;  - Otherwise, "/login-applications"
  ;; Otherwise, flash errors and redirect to "/login
  (d/future
    (let [{:keys [email password]} (get-in ctx [:parameters :form])]
      (let [user (users/fetch-by-email db email)
            valid? (when user (users/valid-password? db (:id user) password))
            tenants (when valid? (tenants/fetch-for-user db (:id user)))]
        (if valid?
          (let [now (time/now)
                exp (time/plus now (time/days 30))
                self-url (format "%s://%s" (:protocol config) (:host config))
                claims {:iss self-url
                        :sub (:id user)
                        :aud self-url
                        :iat (util/unix-time now)
                        :exp (util/unix-time exp)
                        :email (:email user)
                        :tenants (map abridged-tenant tenants)}]
            (-> ctx
                (update-in [:response :cookies] merge
                           {"session" {:value (jwt/sign jwt claims)
                                       :http-only true
                                       :domain (:host config)}
                            ;; Clear redirect cookie
                            "redirect_to" {:value ""
                                           :http-only true
                                           :domain (:host config)
                                           :path "/login"
                                           :max-age 0}})
                (redir-to
                 (get-in ctx [:cookies "redirect_to"]
                         (if (some tenants/is-managing? tenants) "/dashboard" "/login-applications")))))
          (do (println "Invalid credentials:" user)
              ;; TODO: Add flash message
              (redir-to ctx "/login")))))))

(defn routes [config db redis jwt]
  ["/login" (yada/resource
             {:id :sesame.resources/login
              :description "Resource for creating sessions"
              :methods {:get {:produces #{"text/html"}
                              :response #(render-login config %)}
                        :post {:produces #{"text/html"}
                               :consumes "application/x-www-form-urlencoded"
                               :parameters {:form LoginForm}
                               :response #(handle-login config db jwt %)}}})])
