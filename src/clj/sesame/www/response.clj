(ns sesame.www.response
  (:require [clojure.string :as s]
            [selmer.parser :refer [render-file]])
  (:import [java.net URLEncoder]))

(defn default-port? [protocol port]
  (case protocol
    "http" (= port 80)
    "https" (= port 443)
    false))

(defn urlencode [str]
  (URLEncoder/encode str "UTF-8"))

(defn as-string [v]
  (cond
    (keyword? v) (name v)
    (string? v) v
    :else (str v)))

(defn base-url [config]
  (let [{:keys [host protocol port]
         :or {host "localhost"
              protocol "https"
              port 443}} config]
    (str protocol "://" host
         (if (default-port? protocol port) "" (str ":" port)))))

(defn api-path [config path]
  (let [api-version (get config :api-version "v1")]
    (s/join "/" [(base-url config) "api" api-version path])))

(defn with-query-string [url params]
  {:pre (= -1 (.indexOf url "?"))}
  (if (empty? params)
    url
    (str url "?"
         (s/join "&"
                 (for [[key value] params]
                   (str (urlencode (as-string key))
                        "="
                        (urlencode value)))))))

(defn ok [body]
  {:status 200
   :body body})

(defn redirect [path]
  {:status 301
   :headers {"Location" path}})

(defn not-found
  ([] (not-found "Not Found"))
  ([message]
   {:status 404
    :body message}))

(defn not-implemented []
  {:status 501
   :headers {"Content-Type" "text/plain"}
   :body "Method not implemented"})

(defn error-page
  ([errors] (error-page errors 500))
  ([errors status]
   {:status status
    :headers {"Content-Type" "text/html"}
    :body (render-file "views/error.html" {:errors errors})}))

(def oauth-errors #{:invalid_request :invalid_client :invalid_grant
                    :unauthorized_client :unsupported_grant_type :invalid_scope})
(defn oauth-error
  ([code] (oauth-error code nil nil 400))
  ([code desc] (oauth-error code desc nil 400))
  ([code desc uri] (oauth-error code desc uri 400))
  ([code desc uri status]
   {:status status
    :headers {"Content-Type" "application/json"}
    :body (cond-> {:error code}
            desc (assoc :error_description desc)
            uri (assoc :error_uri uri))}))

(defn error-response
  ([msg] (error-response msg 500))
  ([msg status]
   {:status status
    :headers {"Content-Type" "application/json"}
    :body {:status "error"
           :message msg}}))

(comment
  (let [config {:host "auth.example.com" :port 3000}]
    (-> config
        (api-path "oauth/authorize")
        (with-query-string {:client_id "123-456"
                            :secret "super&secret?"
                            "other" "stuff"})
        (redirect))))
