(ns sesame.www.util)


(defn redirect-response
  [ctx url]
  (-> (:response ctx)
      (assoc :status 302)
      (update :headers merge {:location url})))


(defn default-resource [jwt]
  {:access-control {:realms {"default" {:authentication-schemes [{:scheme :sesame.authentication/oauth2
                                                                  :sesame.oauth2/signer jwt
                                                                  :sesame.oauth2/header? true
                                                                  :sesame.oauth2/cookie? true}]
                                        :authorization {:scheme :sesame.authorization/rbac}}}}})
