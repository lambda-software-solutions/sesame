(ns sesame.www.applications
  (:require [yada.yada :as yada]
            [yada.interceptors]
            [yada.handler]
            [manifold.deferred :as d]
            [sesame.domain.applications :as model]
            [sesame.domain.auth :as auth]
            [sesame.www.api :as api]))


(def blacklisted-props [:is_active :is_default :is_system :secret_digest])


(defn create-application [db ctx]
  (d/future
    (let [application (get-in ctx [:parameters :body])
          {:keys [id] :as saved} (model/create-application! db application)
          location (yada/path-for ctx :sesame.resources.applications/application {:route-params {:id id}})]
      (-> (:response ctx)
          (assoc-in [:status] 201)
          (update-in [:headers] conj ["Location" location])
          (assoc-in [:body] saved)))))


(defn get-application [db ctx]
  (d/future
    (model/fetch-by-id db (get-in ctx [:parameters :path :id]))))


(defn routes [config db redis jwt]
  ["/applications"
   [
    [""
     (-> (api/resource jwt
                       {:id :sesame.resources.applications/applications
                        :description "Applications resource for creating and retrieving OAuth 2.0 applications"
                        :sesame.properties/blacklist blacklisted-props
                        :methods {:post {:parameters {:body model/Application}
                                         :response #(create-application db %)}}})
         ;; Require that user is an admin for either a managing tenant or the tenant to which
         ;; this application should belong
         (auth/require-roles :sesame.tenants/managing 'ANY)
         (auth/require-roles :sesame.tenants/tenant
                            [:and :sesame.roles/admin
                             (fn [ident _ ctx]
                               (= (str (get-in ctx [:parameters :body :tenant_id]))
                                  (str (get-in ident [:tenant :id]))))])
         (yada/resource))]

    [["/" :id]
     (-> (api/resource jwt
                       {:id :sesame.resources.applications/application
                        :description "Application resource to view, update, or delete an OAuth 2.0 application"
                        :sesame.properties/blacklist blacklisted-props
                        :methods {:get {:response #(get-application db %)}}})
         (yada/resource))]]])
