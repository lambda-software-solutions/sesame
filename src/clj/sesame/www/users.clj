(ns sesame.www.users
  (:require [yada.yada :as yada]
            [manifold.deferred :as d]
            [sesame.domain.users :as model]
            [sesame.domain.auth :as auth]
            [sesame.www.api :as api])
  (:import [java.util UUID]))


(defn create-user [db ctx]
  (d/future
    (model/create-user! db (:body ctx))))

(defn get-user [db ctx]
  (d/future
    (model/fetch-by-id db (get-in ctx [:parameters :path :id]))))

(defn no-users-exist? [db]
  (not (model/any-exist? db)))


(defn routes [config db redis jwt]
  ["/users"
   [
    [""
     (-> (api/resource jwt
                       {:id :sesame.resources.users/users
                        :description "Users resource"
                        :summary "List or create users globally. Note that this endpoint is only permitted for users in a controlling organization, as it does not consider membership in any tenant. For standare tenants, users must be managed via the /tenants/:id/users endpoint."
                        :methods {:post {:parameters {:body model/User}
                                         :response #(create-user db %)}}})
         (auth/require-roles :sesame.tenants/managing 'ANY)
         (auth/require-roles :always (fn [_ _ _] (no-users-exist? db)))
         (yada/resource))]

    [["/" :id]
     (-> (api/resource jwt
                       {:id :sesame.resources.tenants/tenant
                        :description "User resource for viewing and updating single users"
                        :methods {:get {:parameters {:path {:id UUID}}
                                        :response #(get-user db %)}}})
         (yada/resource))]]])
