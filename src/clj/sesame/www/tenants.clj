(ns sesame.www.tenants
  (:require [yada.yada :as yada]
            [schema.core :as s]
            [manifold.deferred :as d]
            [sesame.domain.uri :as uri]
            [sesame.domain.tenants :as model]
            [sesame.domain.auth :as auth]
            [sesame.www.api :as api])
  (:import [java.util UUID]))

(defn create-tenant [db ctx]
  (d/future
    (let [tenant (model/create-tenant! db (:body ctx))
          id (:id tenant)
          location (yada/path-for ctx :sesame.resources.tenants/tenant {:route-params {:id id}})]
      (-> (:response ctx)
          (assoc-in [:status] 201)
          (update-in [:headers] conj ["Location" location])
          (assoc-in [:body] tenant)))))


(defn get-tenant [db ctx]
  (d/future
    (model/fetch-by-id db (get-in ctx [:parameters :path :id]))))


(defn no-tenants-exist? [db]
  (not (model/any-exist? db)))

(defn routes [config db redis jwt]
  ["/tenants"
   [
    [""
     (-> (api/resource jwt
          {:id :sesame.resources.tenants/tenants
           :description "Tenants resource for creating and retrieving tenants"
           :methods {:post {:parameters {:body model/Tenant}
                            :response #(create-tenant db %)}}})
         (auth/require-roles :sesame.tenants/managing :sesame.roles/admin)
         (auth/require-roles :always (fn [_ _ _] (no-tenants-exist? db)))
         (yada/resource))
     ]

    [["/" :id]
     (-> (api/resource jwt
                       {:id :sesame.resources.tenants/tenant
                        :description "Tenants resource for viewing and updating single tenants"
                        :methods {:get {:parameters {:path {:id UUID}}
                                        :response #(get-tenant db %)}}})
         (auth/require-roles :sesame.tenants/managing 'ANY)
         (auth/require-roles :sesame.tenants/tenant
                            [:and :sesame.roles/admin
                             (fn [ident _ ctx]
                               (= (str (get-in ctx [:parameters :path :id]))
                                  (str (get-in ident [:tenant :id]))))])
         (yada/resource))]]])
