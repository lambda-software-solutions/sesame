(ns sesame.www.oauth2
  (:require [clojure.string :as string]
            [yada.yada :as yada]
            [yada.interceptors]
            [yada.handler]
            [selmer.parser :as s.parser]
            [selmer.filters :as s.filters]
            [schema.core :as schema]
            [manifold.deferred :as d]
            [sesame.infr.jwt :as jwt]
            [sesame.domain.oauth2 :as model]
            [sesame.domain.auth :as auth]
            [sesame.domain.applications :as applications]
            [sesame.domain.uri :as uri]
            [sesame.www.util :as www-util])
  (:import [java.util Base64]))

(s.filters/add-filter! :safe-id #(clojure.string/replace % #"[^-\w]+" "-"))


(defn get-params
  "Since we want to reuse the same interceptors for both a GET and POST request,
  we want to get the parameters from either the query (for GET) or form (for POST)
  without the interceptor caring about the request method"
  [ctx]
  (get-in ctx [:parameters
               (if (= :get (:method ctx)) :query :form)] {}))


(defn assoc-scopes
  "Interceptor to asynchronously load scopes from the database"
  [db ctx]
  (if-let [scopes-str (-> ctx get-params :scope)]
    (let [scopes-str (if (sequential? scopes-str)
                       (string/join " " scopes-str)
                       scopes-str)]
      (d/future
        (assoc-in ctx [:state :scopes]
                  (model/fetch-scopes db (model/parse-scopes scopes-str)))))
    (assoc-in ctx [:state :scopes] '())))


(defn assoc-application
  "Interceptor to fetch an appication from the database asynchronously"
  [db ctx]
  (d/future
    (assoc-in ctx [:state :application]
      (applications/fetch-by-id db (-> ctx get-params :client_id)))))


(defn validate-application-present
  "Ensures that an application is present on the context state"
  [ctx]
  (if (get-in ctx [:state :application])
    ctx
    (d/error-deferred
      (ex-info (format "Application not found for client_id: %s" (-> ctx get-params :client_id))
               {:status 400
                :oath2.error "invalid_client"}))))

(defn validate-request-uri-matches
  "Ensures that the redirect_uri passed in as a parameter matches the one authorized for the application"
  [ctx]
  (let [application (get-in ctx [:state :application])
        uri (-> ctx get-params :redirect_uri)]
    (if (= uri (:redirect_uri application))
      ctx
      (d/error-deferred
       (ex-info "Redirect URL provided does not match the one registered for the application"
                {:status 400
                 :oath2.error "invalid_request"})))))


(defn parse-client-creds
  "Get HTTP Basic-parsed credentials, and fall back to crendentials passed as
  form parameters if no HTTP Basic auth headers are present"
  [ctx]
  (or (get-in ctx [:authentication "oauth2"])
      {:client_id (get-in ctx [:parameters :form "client_id"])
       :client_secret (get-in ctx [:parameters :form "client_secret"])}))


(defn redirect-to-login-if-logged-out [config ctx]
  (if (auth/is-req-authenticated? ctx)
    ctx
    (let [login-url (yada/path-for ctx :sesame.resources/login)
          {:keys [uri query-string]} (:request ctx)
          {:keys [host port protocol]} config
          domain (str host (when-not (contains? #{80 443} port)
                             (str ":" port)))
          redir-uri (format "%s://%s%s?%s" protocol domain uri query-string)]
      (-> ctx
          (assoc-in [:response :status] 302)
          (update-in [:response :cookies] merge {"redirect_to" {:value redir-uri
                                                                :http-only true
                                                                :domain host
                                                                :path "/login"}})

          (update-in [:response :headers] merge {:location login-url})))))


(defn authorization-error
  "Generate a redirect response that adds the OAuth 2.0 error parameters to the redirect URI."
  [ctx redirect-uri code description]
  (www-util/redirect-response
   ctx (model/add-error-to-qs redirect-uri code description)))


(defn authorize
  "Handler that serves the OAuth2 authorization page"
  [config db redis ctx]
  (let [{:keys [application scopes]} (:state ctx)
        {:keys [client_id response_type redirect_uri state]} (get-in ctx [:parameters :query])
        scopes-by-display (group-by :display scopes)
        sub (auth/subject-of ctx)
        nonce (model/make-form-nonce! redis client_id sub)]
    (s.parser/render-file "views/authorize/authorize.html"
                          {:app application
                           :config config
                           :scopes (get scopes-by-display true)
                           :scopes-hidden (get scopes-by-display false)
                           :state state
                           :nonce nonce
                           :client-id client_id
                           :redirect-uri redirect_uri
                           :response-type response_type})))


(defn redirect-to-client [config redis ctx]
  (let [{:keys [authorized nonce state client_id redirect_uri response_type scope]}
        (get-in ctx [:parameters :form])
        
        sub (auth/subject-of ctx)
        error-resp (partial authorization-error ctx redirect_uri)]
    (if authorized
      (if (model/nonce-valid? redis nonce client_id sub)
        (let [code (model/make-code! redis client_id redirect_uri scope)]
          (www-util/redirect-response
           ctx (uri/merge-query-params redirect_uri
                                       (cond-> {:code code}
                                         (some? state) (assoc :state state) ))))
        (error-resp "invalid_request" "Request timeout. Please try again"))
      (error-resp "access_denied" "The resource owner denied the request"))))


;; Handler that serves the OAuth2 token endpoint
(defmulti token
  (fn [config db redis jwt ctx]
    (get-in ctx [:parameters :form :grant_type])))

(defmethod token "authorization_code"
  [config db redis jwt ctx]
  (d/future
    (let [{:strs [client_id client_secret code redirect_uri nonce]} (get-in ctx [:parameters :form])]
      (if-let [{:keys [scope] :as data} (model/check-auth-code redis code client_id redirect_uri)]
        (let [exp 3600
              app (applications/fetch-by-id db client_id)
              {:keys [access refresh]} (model/make-access-tokens! config db jwt app exp)
              id-claims (model/retrieve-id-token-claims redis code access)]
          (cond->
              {:access_token access
               :token_type "bearer"
               :expires_in exp
               :id_token (jwt/sign jwt id-claims)}
            (.contains scope "offline_access") (assoc :refresh_token refresh)))
        (throw (ex-info "Invalid or expired authorization code"
                        {:status 400
                         :oath2.error "invalid_client"}))))))

(defmethod token "client_credentials"
  [config db _ jwt ctx]
  (let [{:keys [client_id client_secret]} (parse-client-creds ctx)]
    (d/future
      (if (applications/valid-secret? db client_id client_secret)
        (let [application (applications/fetch-by-id db client_id)
              exp 3600 ;; TODO: Make configurable - maybe load from application and default to 3600
              tokens (model/make-access-tokens! config db jwt application exp)]
          {:access_token (:access tokens)
           :token_type "bearer"
           :expires_in exp})
        (throw (ex-info "Client authentication failed"
                        {:status 400
                         :oauth2.error "invalid_client"}))))))
  
(defmethod token :default
  [_ _ _ _ ctx]
  (throw
   (if-let [grant-type (get-in ctx [:parameters :form :grant_type])]
     ;; This branch will only be hit in the event of a code bug, as the parameter
     ;; schema will catch all non-conforming grant types
     (ex-info (format "Unsupported grant_type: %s" grant-type)
              {:status 400
               :oauth2.error "unsupported_grant_type"})
     (ex-info "Missing grant_type"
              {:status 400
               :oauth2.error "invalid_request"}))))


(defn verify [config db jwt ctx]
  (let [token (get-in ctx [:parameters :body :token])
        details (model/get-token-details db jwt token)]
    (cond-> {:is_valid (boolean details)}
      details (assoc :details details))))


;; TODO: Add error interceptor that converts oauth2 errors to error responses
(defn routes [config db redis jwt]
  ["/oauth2"
   [
    ["/authorize"
     (-> {:id :sesame.resources.oauth2/authorize
          :description "OAuth 2.0 authorization endpoint. User agents should redirect here so that the resource owner can authorize access."
          :interceptor-chain yada/default-interceptor-chain
          :methods {:get {:parameters {:query model/AuthorizeParams}
                          :produces #{"text/html"}
                          :response #(authorize config db redis %)}
                    :post {:consumes #{"application/x-www-form-urlencoded"}
                           :parameters {:form (-> model/AuthorizeParams
                                                  (dissoc (schema/optional-key :scope))
                                                  (assoc :authorized Boolean
                                                         :scope [String]))}
                           :produces #{"text/html"}
                           :response #(redirect-to-client config redis %)}}}
         (merge (www-util/default-resource jwt))
         (yada/resource)
         (yada.handler/append-interceptor
          yada.interceptors/get-properties
          #(redirect-to-login-if-logged-out config %))
         (yada.handler/append-interceptor
          yada.interceptors/process-request-body
          #(assoc-scopes db %)
          #(assoc-application db %)
          validate-application-present
          validate-request-uri-matches))]

    ["/token"
     (-> (yada/resource
          {:id :sesame.resources.oauth2/token
           :description "OAuth 2.0 token endpoint. Authorized applications should obtain an access token here."
           :access-control {:realm "oauth2"
                            :scheme "Basic"
                            :verify (fn [[user password]]
                                      (when (and user password)
                                        {:client_id user
                                         :client_secret password}))}
           :methods {:post {:parameters {:form model/TokenParams
                                         }
                            :consumes "application/x-www-form-urlencoded"
                            :produces #{"application/json" "application/edn"}
                            :response #(token config db redis jwt %)}}}))]

    ["/verify"
     (-> (yada/resource
          {:id :sesame.resources.oauth2/verify
           :description "Verify authorization tokens issued by this server. Any type of token can be validated here, even though some types (e.g. JWT) can be verified service-side."
           :methods {:post {:parameters {:body {:token String}}
                            :consumes #{"application/json" "application/edn"}
                            :produces #{"application/json" "application/edn"}
                            :response #(verify config db jwt %)}}}))]]])

