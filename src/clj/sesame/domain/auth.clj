(ns sesame.domain.auth
  (:require [taoensso.carmine :as car :refer [wcar]]
            [taoensso.timbre :as timbre :refer [info debug error]]
            [yada.security :refer [verify]]
            [yada.authorization :refer [validate]]
            [clj-time.core :as time]
            [sesame.util :as util]
            [sesame.domain.crypto :as crypto]
            [sesame.infr.jwt :as jwt]
            [sesame.domain.applications :as applications]
            [sesame.domain.oauth2 :as oauth2]))

;; This should probably be mostly in the oauth2 namespace, and some of it is
;; probably more appropriate for the service layer

(def five-minutes (* 60 5))

(def id-token-valid-period (time/minutes 10))

(defn code-key [code]
  (str "auth-codes:" code))

(defn id-token-key [code]
  (str "id-tokens:" code))

(defn make-code
  "Store code in redis for five minutes. Key is hash of generated code,
   and value is a map with keys :client-id, :redirect-url, and :scope"
  [redis client-id redirect-uri scope]
  (let [code (crypto/auth-code)
        value {:client-id client-id
               :redirect-uri redirect-uri
               :scope scope}]
    (wcar redis
          (car/setex (code-key code) five-minutes value))
    code))

(defn gen-tokens []
  {:access (crypto/auth-code)
   :refresh (crypto/auth-code)})

(defn make-access-tokens
  "Creates a structure containing the access and refresh tokens for a client-id
  and stores in redis."
  [redis client-id exp]
  (let [{:keys [access] :as tokens} (gen-tokens)]
    (wcar redis
          (car/setex (code-key access) exp
                     {:client-id client-id
                      :tokens tokens}))
    tokens))

(defn gen-id-token [config redis client-id {:keys [nonce]} code]
  (let [now (time/now)
        exp (time/plus now id-token-valid-period)
        claims {:iss (format "%s://%s" (:protocol config) (:host config))
                :sub ((crypto/key-of-length 32))
                :aud client-id
                :nonce nonce
                :iat (util/unix-time now)
                :exp (util/unix-time exp)
                :auth_time (util/unix-time now)}]
    (wcar redis (car/setex (id-token-key code)
                           (get config :id-token-valid-sec (* 15 60))
                           claims))))

; (defn authorize [config db redis params]
;   (let [{:keys [client-id redirect-uri response-type scope state]} params
;         c (applications/fetch-by-id db client-id)]
;     (cond
;       (nil? client-id)
;       (resp/oauth-error :invalid_client "Client identifier is required")
;
;       (nil? c)
;       (resp/oauth-error :invalid_client "Client not found")
;
;       (not= redirect-uri (:redirect-uri c))
;       (resp/oauth-error :invalid_request "Callback URI does not match client")
;
;       :else
;       (let [code (make-code redis client-id redirect-uri scope)]
;         (when (.contains scope "openid")
;           (gen-id-token config redis client-id params code))
;         (-> redirect-uri
;             (resp/with-query-string
;               (cond-> {:code code}
;                 state (assoc :state state)))
;             (resp/redirect))))))

(defn check-auth-code [redis code client-id redirect-uri]
  (when-let [data (wcar redis (car/get (code-key code)))]
    (when (and (= client-id (:client-id data))
               (= redirect-uri (:redirect-uri data)))
      data)))

(defmulti get-token (fn [config redis jwt params] (:grant-type params)))

; (defmethod get-token "authorization_code"
;   [config redis jwt {:keys [client-id client-secret code redirect-uri nonce]}]
;   (if-let [{:keys [scope] :as data} (check-auth-code redis code client-id redirect-uri)]
;     (let [exp 3600
;           {:keys [access refresh]} (make-access-tokens redis client-id exp)
;           claims (wcar redis (car/get (id-token-key code)))
;           id-claims (assoc claims :at_hash (crypto/at-hash access))]
;       (resp/ok
;        (cond->
;            {:access_token access
;             :token_type "bearer"
;             :expires_in exp
;             :id_token (jwt/sign jwt id-claims)}
;          (.contains scope "offline_access")
;          (assoc :refresh_token refresh))))
;     (resp/oauth-error :invalid_grant "Invalid or expired authorization code")))

; (defmethod get-token :default
;   [_ _ _ {:keys [grant-type]}]
;   (if (nil? grant-type)
;     (resp/oauth-error :invalid_request "Grant type must be secified")
;     (resp/oauth-error :unsupported_grant_type "Unknown grant type")))

(defn is-req-authenticated? [ctx]
  (some? (get-in ctx [:authentication "default" :identity])))

(defn subject-of
  "Extracts the subject (user id) from the client JWT - used to securely
  associate pieces of information with a particular user."
  [ctx]
  (get-in ctx [:authentication "default" :identity :sub]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Interceptors / Auth Middleware

;; TODO: Handle this using Schema coersions
(defn coerce-tenant [t]
  (-> t
      (update-in [:type] keyword)
      (update-in [:roles] #(mapv keyword %))))


(defn- verify-impl [ctx {:keys [sesame.oauth2/signer
                                sesame.oauth2/cookie
                                sesame.oauth2/cookie?
                                sesame.oauth2/header?]
                        :as params}]
  (when-not signer
    (throw (ex-info "JWT signer not provided" {:params params})))
  (when-not (or cookie? header?)
    (throw (ex-info "Must allow at least one of cookie or header for auth" {:params params})))
  (let [tokens (cond-> []
                 cookie? (conj (get-in ctx [:cookies (or cookie "session")]))
                 header? (conj (oauth2/parse-header
                                (get-in ctx [:request :headers "Authorization"] ""))))]
    (if-let [token (first (filter some? tokens))]
      (try
        {:identity (-> (jwt/unsign signer token)
                       (update-in [:tenants] #(map coerce-tenant %)))}
        (catch clojure.lang.ExceptionInfo e
          {:error (ex-data e)})
        (catch Exception e
          (error e "Unknown error unsigning JWT")
          {:error {:type :exception :cause (.getMessage e)}}))
      {:error {:type :validation :cause :token-missing}})))


(defmethod verify
  :sesame.authentication/oauth2
  [ctx params]
  (verify-impl ctx params))


;;;;;;;;;;;;;;;;;;;;
;; Resource mutation

(defn require-authenticated
  "Update the authorization tree to prepend a predicate function requiring
  that the request be authenticated"
  [resource]
  (assoc-in resource
            [:access-control :realms "default" :authorization :sesame.authorization/authenticated?]
            true))

(defn with-custom-validation
  "Update the authorization tree to provide a custom authorization function. This function
  will be called as: (validate roles identity params ctx) where roles is the role tree, identity
  is the authenticated user's decoded session JWT params are any additional params supplied
  to the auth method, and ctx is the yada context. A (possibly updated) ctx should be returned on
  authorization success, and nil should be returned otherwise."
  [resource validate]
  (assoc-in resource
            [:access-control :realms "default" :authorization :sesame.authorization/validate]
            validate))


(defn require-roles [resource tenant-type role-spec]
  (assoc-in resource
            [:access-control :realms "default" :authorization :sesame.authorization/roles tenant-type]
            role-spec))


;; TODO: Move to an rbac namespace
(defn eval-for-tenant
  "Evaluate a role-spec for a single tenant to determine if a user is authorized
  by virtue of their membership in that tenant."
  ^Boolean [node tenant params ctx path]
  (cond
    ;; Base case 1:
    ;; Missing role spec should evaluate to true
    (nil? node) true

    ;; Base case 2:
    ;; Keywords represent a role that is expected to be present on the identified user
    (keyword? node) (as-> (:roles tenant) $
                      (into #{} $)
                      (contains? $ node))
    
    ;; Base case 3:
    ;; Symbols have special pre-defined meaning in the language
    (symbol? node) (condp = node
                     'ANY true
                     'NONE false
                     (do (error (format "Unknown symbol, %s at: %s" (name node) (pr-str path)))
                         true))

    ;; Base case 3:
    ;; Functions evaluate to their application to the identity, auth params, and context
    (fn? node) (boolean (node tenant params ctx))

    ;; Recursive Case:
    ;; Vectors represented as [pred & args] are evaluated as boolean predicates
    (vector? node) (let [[pred & args] node
                         evaled-args (map-indexed (fn [idx next-node]
                                                    (eval-for-tenant next-node tenant params ctx
                                                                (into [] (concat path [pred idx]))))
                                                  args)]
                     (case pred
                       ;; Children are evaluated according to default truthiness semantics
                       :and (every? boolean evaled-args)
                       :or (boolean (some identity evaled-args))
                       :not (-> evaled-args first not)))

    ;; Error case:
    :else (do (error (format "Unknown node type, %s at: %s" (type node) (pr-str path)))
              true)))


(defn eval-roles
  ^Boolean [spec ident params ctx]
  (let [{spec-managing :sesame.tenants/managing
         spec-tenants :sesame.tenants/tenant
         always :always} spec
        {:keys [tenants]} ident
        {ts-managing :sesame.tenants/managing
         ts-tenant :sesame.tenants/tenant} (group-by :type tenants)]
    (or
     (empty? spec)
     (some true?
           (concat
            (map #(eval-for-tenant spec-managing % params ctx [:sesame.tenants/managing]) ts-managing)
            (map #(eval-for-tenant spec-tenants % params ctx [:sesame.tenants/tenant]) ts-tenant)
            ;; Can specify some function of identity, auth params and context to always evaluate.
            ;; This allows us to account for situations with authorization depends on something
            ;; out of band from the normal RBAC system (e.g. allowing creation of a the first tenant
            ;; by anyone)
            [(when always (always ident params ctx))])))))


(defn validate-impl [ctx
                     {:keys [identity error] :as creds}
                     {:keys [sesame.authorization/validate ; Validation fn override
                             sesame.authorization/roles
                             sesame.authorization/authenticated?]
                      :as params}]
  (let [roles (or roles [])]
    (if validate
      (validate roles identity params ctx)
      (when-not (and authenticated? (nil? identity))
        (when (eval-roles roles identity params ctx)
          ctx)))))


(defmethod validate
  :sesame.authorization/rbac
  [ctx credentials authorization]
  (validate-impl ctx credentials authorization))
