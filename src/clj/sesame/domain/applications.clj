(ns sesame.domain.applications
  (:require [schema.core :as s]
            [clojure.java.jdbc :as jdbc]
            [sesame.domain.crypto :as crypto]
            [sesame.domain.uri :as uri]))

(defn valid-token-type? [t]
  (contains? #{"bearer" "jwt"} t))

(def Application
  {:name String
   :tenant_id s/Uuid
   (s/optional-key :description) String
   (s/optional-key :image) (s/constrained String uri/is-absolute-uri?)
   (s/optional-key :redirect_uri) (s/constrained String uri/is-absolute-uri?)
   (s/optional-key :token_type) (s/constrained String valid-token-type?)})


(defn create-application! [db application]
  (let [id (crypto/api-key)
        secret (crypto/api-secret)
        digest (crypto/hash-secret secret)
        row (assoc application :id id :secret_digest digest)]
    (assoc
     (first (jdbc/insert! db "application" row))
     :secret secret)))


(defn create-auth-code! [db id]
  (let [code (crypto/auth-code)
        digest (crypto/hash-secret code)]
    (jdbc/insert! db "code" {:client_id id
                             :code_digest digest})
    {:code code}))


(defn db-query [db sql & args]
  (jdbc/query db (conj args sql)))


(defn fetch-by-id [db id]
  (first
   (db-query db "SELECT * from application WHERE id = ?" id)))


(defn fetch-for-tenant [db tenant-id]
  (db-query db "SELECT * from application WHERE tenant_id = ?" tenant-id))


(defn valid-secret? [db id secret]
  (not
   (empty?
    (jdbc/query db
      ["SELECT 1 FROM application WHERE id = ? AND secret_digest = ?" id (crypto/hash-secret secret)]))))
