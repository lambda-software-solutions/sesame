(ns sesame.domain.uri
  (:require [ring.util.codec :as url-codec]
            [clojure.string :as string])
  (:import [java.net URI]))

(defn is-absolute-uri? [^String s]
  (try
    (.isAbsolute (URI. s))
    (catch Exception e
      false)))


(defn has-query-string? [url]
  (> (.indexOf url "?") -1))


(defn as-string [v]
  (cond
    (keyword? v) (name v)
    (symbol? v) (name v)
    (string? v) v
    :else (str v)))

(defn parse-query-params
  "Gets a map of decoded query string paramters from a given url"
  ([url] (parse-query-params url true))
  ([url keys-as-kw?]
   (if (has-query-string? url)
     (let [idx (inc (.indexOf url "?"))
           qs (.substring url idx)
           segments (string/split qs #"&")]
       (reduce (fn [acc segment]
                 (let [[k v] (map url-codec/url-decode (string/split segment #"="))]
                   (assoc acc (if keys-as-kw? (keyword k) k) v)))
               {} segments))
     {})))


(defn append-query-string [url params]
  {:pre [(not (has-query-string? url))]}
  (if (empty? params)
      url
      (str url "?"
           (string/join "&"
                   (for [[key value] params]
                     (str (url-codec/url-encode (as-string key))
                          "="
                          (url-codec/url-encode value)))))))


(defn strip-query-string [url]
  (if (has-query-string? url)
    (.substring url 0 (.indexOf url "?"))
    url))


(defn merge-query-params [url params]
  (let [original-params (parse-query-params url)]
    (-> url
        (strip-query-string)
        (append-query-string (merge original-params params)))))
