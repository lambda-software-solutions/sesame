(ns sesame.domain.oauth2
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [clj-time.core :as time]
            [taoensso.carmine :as car :refer [wcar]]
            [schema.core :as s]
            [sesame.util :as util]
            [sesame.infr.jwt :as jwt]
            [sesame.domain.uri :as uri]
            [sesame.domain.crypto :as crypto]
            [sesame.domain.openid :as oidc]))


(def five-minutes (* 60 5))


(def id-token-valid-period (time/minutes 10))


(defn- prefix-fn [prefix]
  (fn [s] (str prefix s)))

(def code-key (prefix-fn "auth-codes:"))
(def id-token-key (prefix-fn "id-tokens:"))
(def nonce-key (prefix-fn "nonces:"))


(defn valid-response-type? [t]
  (contains? #{"code" "token" "id_token"} t))


(defn valid-grant-type? [t]
  (contains? #{"authorization_code" "password" "client_credentials" "refresh_token"} t))


(def AuthorizeParams
  {;; OAuth 2.0 Params
   :client_id String
   :response_type (s/constrained String valid-response-type?)
   (s/optional-key :redirect_uri) (s/constrained String uri/is-absolute-uri?)
   (s/optional-key :scope) String
   (s/optional-key :state) String
   ;; OpenId Connect 1.0 params
   (s/optional-key :nonce) String
   (s/optional-key :display) (s/constrained String oidc/valid-display-param?)
   (s/optional-key :prompt) (s/constrained String oidc/valid-prompt?)
   (s/optional-key :max_age) Long
   (s/optional-key :ui_locales) String
   (s/optional-key :id_token_hint) String
   (s/optional-key :login_hint) String
   (s/optional-key :acr_values) String})


(def TokenParams
  {:grant_type (s/constrained String valid-grant-type?)
   (s/optional-key :client_id) String
   (s/optional-key :client_secret) String
   (s/optional-key :state) String
   (s/optional-key :code) String
   (s/optional-key :nonce) String
   (s/optional-key :redirect_uri) (s/constrained String uri/is-absolute-uri?)})


(defmulti gen-tokens
  "Creates a pair of access and refresh tokens according to the application's
  token type"
  (fn [config jwt application exp] (:token_type application)))

(defmethod gen-tokens "bearer"
  [_ _ _ _]
  {:access (crypto/auth-code)
   :refresh (crypto/auth-code)})

(defmethod gen-tokens "jwt"
  [config jwt application exp]
  (let [now (util/unix-time (time/now))]
    {:access (jwt/sign jwt
                       {:iss (format "%s://%s" (:protocol config) (:host config))
                        :sub (:id application)
                        :iat now
                        :exp (+ now exp)})
     :refresh (crypto/auth-code)}))


(defn make-code!
  "Store code in redis for five minutes. Key is hash of generated code,
   and value is a map with keys :client-id, :redirect-url, and :scope"
  [redis client-id redirect-uri scope]
  (let [code (crypto/auth-code)
        value {:client-id client-id
               :redirect-uri redirect-uri
               :scope scope}]
    (wcar redis
          (car/setex (code-key code) five-minutes value))
    code))

(defn make-form-nonce!
  "Generates a random value and stores it in the database that is checked on
  authorization to help prevent a cross-site scripting attach from being able
  to craft a custom authorization request"
  ([redis client-id user-id]
   (make-form-nonce! redis client-id user-id five-minutes))
  ([redis client-id user-id max-age]
   (let [nonce (crypto/auth-code)
         value {:client-id client-id
                :user-id user-id}]
     (wcar redis
           (car/setex (nonce-key nonce) max-age value))
     nonce)))


(defn nonce-valid?
  "Validates a nonce created with `make-form-nonce!`"
  [redis nonce client-id user-id]
  (when-let [data (wcar redis (car/get (nonce-key nonce)))]
    (and (= client-id (:client-id data))
         (= user-id (:user-id data)))))


;; TODO: Store scope with token
(defn make-access-tokens!
  [config db jwt app exp]
  (let [{:keys [id token_type]} app
        tokens (gen-tokens config jwt app exp)
        token-digest (crypto/hash-secret (:access tokens))
        refresh-digest (crypto/hash-secret (:refresh tokens))]
    (jdbc/with-db-transaction [tx db]
      (jdbc/execute! tx [(format "INSERT INTO access_token (token_digest, token_type, application_id, exp) VALUES (?, ?, ?, now() + interval '%d seconds')" exp)
                         token-digest token_type id])
      (jdbc/insert! tx "refresh_token"
                    {:token_digest refresh-digest
                     :access_token_digest token-digest
                     :application_id id}))
    tokens))


(defn gen-id-token [config redis client-id {:keys [nonce]} code identity]
  (let [now (time/now)
        exp (time/plus now id-token-valid-period)
        user-id (:id identity)
        claims {:iss (format "%s://%s" (:protocol config) (:host config))
                :sub user-id
                :aud client-id
                :nonce nonce
                :iat (util/unix-time now)
                :exp (util/unix-time exp)
                :auth_time (util/unix-time now)}]
    (wcar redis (car/setex (id-token-key code)
                           (get config :id-token-valid-sec (* 15 60))
                           claims))
    claims))


(defn retrieve-id-token-claims
  "Retrieve the claims for and OpenID Connect id_token from the claims
  previously stored for the given code and an at_hash generated from the
  access_token"
  [redis code access]
  (when-let [claims (wcar redis (car/get (id-token-key code)))]
    (assoc claims :at_hash (crypto/at-hash access))))


(defn check-auth-code [redis code client-id redirect-uri]
  (when-let [data (wcar redis (car/get (code-key code)))]
    (when (and (= client-id (:client-id data))
               (= redirect-uri (:redirect-uri data)))
      data)))

(defn get-token-details [db jwt token]
  ;; Try to parse token as JWT, then fallback to database lookup
  (try
    (jwt/unsign jwt token)
    (catch Exception e
      ;; Fallback to db lookup
      (first
       (jdbc/query db ["SELECT token_type, is_revoked, application_id, floor(extract('epoch' from exp)) AS exp, floor(extract('epoch' from nbf)) AS nbf FROM access_token WHERE token_digest = ? AND nbf <= now() AND exp > now()"
                       (crypto/hash-secret token)])))))


(defn parse-header [auth-header]
  (when (string/starts-with? auth-header "Bearer ")
    (.substring auth-header 7)))


(defn parse-scopes [scopes-str]
  (seq (string/split scopes-str #"\s+")))


(defn fetch-scopes [db scopes]
  (let [sql (str "SELECT * FROM \"scope\" WHERE \"scope\".\"scope\" IN ("
                 (string/join "," (map (constantly "?") scopes))
                 ")")]
    (jdbc/query db (conj scopes sql))))


(defn add-error-to-qs
  "Adds OAuth 2.0 error parameters to a redirect_uri"
  ([url code] (add-error-to-qs url code nil nil))
  ([url code description] (add-error-to-qs url code description nil))
  ([url code description error-uri]
   (uri/merge-query-params url
                           (cond-> {:error code}
                             description (assoc :description description)
                             error-uri (assoc :error_uri error-uri)))))
