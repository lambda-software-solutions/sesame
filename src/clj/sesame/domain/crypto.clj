(ns sesame.domain.crypto
  (:require [buddy.core.hash :as hash])
  (:import [java.security SecureRandom]
           [java.nio.charset Charset]
           [java.util Base64]))

(def default-str-charset
  (Charset/forName "UTF-8"))

(defn random-bytes [bytes]
  (let [rn (SecureRandom.)
        buff (byte-array bytes)]
    (.nextBytes rn buff)
    buff))

(defn base64encode [bytes]
  (let [encoder (.withoutPadding (java.util.Base64/getUrlEncoder))]
    (.encodeToString encoder bytes)))

(defn key-of-length [byte-len]
  (fn [] (base64encode (random-bytes byte-len))))

(def ^{:doc "Returns a 32-character secure random API key that is URL-safe"}
  api-key (key-of-length 24))
(def api-secret (key-of-length 64))
(def auth-code (key-of-length 16))

(defn hash-secret
  "Get the SHA-256 hash of some secret value, encoded as a base64 string.
  A slower hash such as bcrypt or pbkdf2should be used for password hashing."
  [secret]
  (-> secret hash/sha256 base64encode))

(defn validate-secret [secret digest]
  (= (hash-secret secret)
     digest))

(defn at-hash
  "Returns the first half of an access token hash as defined by the
  OpenID Connect spec: http://openid.net/specs/openid-connect-core-1_0.html#CodeIDToken"
  [token]
  (->> token
       (hash/sha256)
       (take 16)
       (byte-array)
       (base64encode)))
