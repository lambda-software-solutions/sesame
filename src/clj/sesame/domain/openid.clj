(ns sesame.domain.openid)

(defn valid-display-param? [param]
  (contains? #{"page" "popup" "touch" "wap"} param))


(defn valid-prompt? [prompt]
  (contains? #{"none" "login" "consent" "select_account"} prompt))
