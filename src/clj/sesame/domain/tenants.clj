(ns sesame.domain.tenants
  (:require [clojure.java.jdbc :as jdbc]
            [schema.core :as s]
            [clj-uuid :as uuid]
            [sesame.domain.addresses :as addr]
            [sesame.domain.localization :as l10n]))

(def Tenant
  {:name String
   (s/optional-key :website) String
   (s/optional-key :zoneinfo) (s/constrained String l10n/is-valid-timezone?)
   (s/optional-key :locale) (s/constrained String l10n/is-valid-locale?)
   (s/optional-key :phone_number) String
   (s/optional-key :address) addr/Address})


(defn set-type [tenant]
  (assoc tenant :type
         (if (:is_managing tenant)
           :sesame.tenants/managing
           :sesame.tenants/tenant)))

(defn display [tenant]
  (-> tenant
      (addr/nest-address)
      (set-type)
      (dissoc :is_managing)))


(defn is-managing? [tenant]
  (= :sesame.tenants/managing (:type tenant)))


(defn any-exist? [db]
  (some-> (jdbc/query db ["SELECT EXISTS (SELECT 1 FROM tenant LIMIT 1) AS has_rows"])
          first
          :has_rows))


(defn make-managing! [db tenant-id]
  (jdbc/update! db "tenant"
                {:is_managing true}
                ["id = ?" tenant-id]))


(defn fetch-by-id [db id]
  (some-> (jdbc/query db ["SELECT * from tenant WHERE id = ?" id])
          (first)
          (display)))


(defn fetch-for-user [db user-id]
  (->> (jdbc/query db ["SELECT t.*, array_agg(tu.role) AS roles FROM tenant t INNER JOIN tenant_user tu ON t.id = tu.tenant_id WHERE tu.user_id = ? GROUP BY t.id" user-id])
       (map display)))


(defn create-tenant! [db tenant]
  (let [id (uuid/v1)
        tenant (-> tenant
                   (assoc :id id)
                   (addr/merge-address))]
    (jdbc/with-db-transaction [tx db {:isolation :read-committed}]
      (let [is-first-tenant? (not (any-exist? tx))]
        (-> (jdbc/insert! tx "tenant" tenant)
            (first)
            (addr/nest-address))
        (when is-first-tenant?
          (make-managing! tx id)))
      (fetch-by-id tx id))))
