(ns sesame.domain.users
  (:require [clojure.java.jdbc :as jdbc]
            [buddy.hashers :as hashers]
            [schema.core :as s]
            [clj-uuid :as uuid]
            [sesame.domain.addresses :as addr]
            [sesame.domain.uri :as uri]
            [sesame.domain.localization :as l10n]))

;; Random note - JWT Format should include:
;; {:id "some-uuid"
;;  :tenant {:id "some-uuid"
;;           :roles [...]
;;           :type :sesame.tenants/tenant}}

(def default-provider-id 1)


(def User
  {:email String
   :password String
   :given_name String
   (s/optional-key :family_name) String
   (s/optional-key :middle_name) String
   (s/optional-key :nickname) String
   (s/optional-key :picture) (s/constrained String uri/is-absolute-uri?)
   (s/optional-key :website) String
   (s/optional-key :gender) String
   (s/optional-key :birthdate) String
   (s/optional-key :zoneinfo) (s/constrained String l10n/is-valid-timezone?)
   (s/optional-key :locale) (s/constrained String l10n/is-valid-locale?)
   (s/optional-key :phone_number) String
   (s/optional-key :address) addr/Address})


(defn any-exist? [db]
  (some-> (jdbc/query db ["SELECT EXISTS (SELECT 1 FROM \"user\" LIMIT 1) AS has_rows"])
          first
          :has_rows))


(defn- fetch-one-by [col]
  (fn [db val]
    (let [sql (format "SELECT * FROM \"user\" WHERE \"%s\" = ? LIMIT 1" col)]
      (-> (jdbc/query db [sql val])
          (first)
          (addr/nest-address)))))


(def fetch-by-id (fetch-one-by "id"))
(def fetch-by-email (fetch-one-by "email"))


(defn create-user! [db user]
  (let [id (uuid/v1)
        password-digest (hashers/derive (:password user))
        user (-> user
                 (assoc :id id)
                 (dissoc :password)
                 (addr/merge-address))]
    (jdbc/with-db-transaction [tx db {:isolation :read-committed}]
      (let [is-first-user? (not (any-exist? tx))]
        ;; Table name needs to be quoted because it is a keyword in postgres, and JDBC will not escape
        (jdbc/insert! tx "\"user\"" user)
        (jdbc/insert! tx "identity" {:user_id id
                                     :provider_id default-provider-id
                                     :token_type "password"
                                     :token password-digest})
        ;; If this is the first user in the system, associate them with the first tenant as an admin
        (when is-first-user?
          (jdbc/execute! tx ["INSERT INTO tenant_user (tenant_id, user_id, role) VALUES ((SELECT id FROM tenant LIMIT 1), ?, ?)" id, "sesame.roles/admin"]))))
    (fetch-by-id db id)))


(defn valid-password? [db user-id password]
  (let [digest (-> (jdbc/query db ["SELECT token FROM identity WHERE provider_id = ? AND token_type = ? AND user_id = ?" default-provider-id "password" user-id])
                   first
                   :token)]
    (hashers/check password digest)))

