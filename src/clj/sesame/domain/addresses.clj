(ns sesame.domain.addresses
  (:require [schema.core :as s]))

(def Address
  {(s/optional-key :street_address) String
   (s/optional-key :locality) String
   (s/optional-key :region) String
   (s/optional-key :postal_code) String
   (s/optional-key :country) String})

(def address-props
  [:street_address :locality :region :postal_code :country])

(defn nest-address [model]
  (let [without-addr (apply dissoc model address-props)
        address (->> (select-keys model address-props)
                     (filter second)
                     (into {}))]
    (assoc without-addr :address address)))

(defn merge-address [model]
  (let [address (:address model)]
    (-> model
        (dissoc :address)
        (merge address))))
