(ns sesame.domain.localization
  (:import [sun.util.calendar ZoneInfo]
           [java.util Locale]))

(def timezones (into #{} (ZoneInfo/getAvailableIDs)))

(defn is-valid-timezone? [tz]
  (contains? timezones tz))

(def locales (into #{} (map str (into [] (Locale/getAvailableLocales)))))

(defn is-valid-locale? [locale]
  (contains? locales locale))
