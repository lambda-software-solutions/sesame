(ns user
  (:require [reloaded.repl :refer [system init start stop go reset]]
            [clojure.pprint :as pprint]
            [ragtime.jdbc :as rjdbc]
            [ragtime.repl :as ragtime]
            [sesame.system :as sys]))

(reloaded.repl/set-init! sys/system)

(defn migration-config []
  {:datastore (rjdbc/sql-database (get-in system [:db :conn]))
   :migrations (rjdbc/load-resources "migrations")})

(defn migrate-db []
  (ragtime/migrate (migration-config)))
(defn rollback-db []
  (ragtime/rollback (migration-config)))

(comment
  "Common operations that we run on the system"

  (go)
  (reset)

  (migrate-db)
  (rollback-db))
