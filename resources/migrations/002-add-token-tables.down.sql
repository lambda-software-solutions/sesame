DROP TRIGGER revoke_refresh_token_on_access_token_revoke ON access_token;
DROP FUNCTION revoke_refresh_token();
DROP TABLE refresh_token;
DROP TABLE access_token;
ALTER TABLE "application" DROP COLUMN "token_type";
