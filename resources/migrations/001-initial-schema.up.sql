CREATE EXTENSION "uuid-ossp";

CREATE OR REPLACE FUNCTION update_updated_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TABLE "user" (
  id UUID PRIMARY KEY,
  email text UNIQUE,
  email_verified boolean default FALSE,
  given_name text,
  family_name text,
  middle_name text,
  nickname text,
  picture text,
  website text,
  gender text,
  birthdate text,
  zoneinfo text,
  locale text,
  phone_number text,
  phone_number_verified boolean,
  street_address text,
  locality text,
  region text,
  postal_code text,
  country text,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);
CREATE TRIGGER update_user_updated_at BEFORE UPDATE ON "user" FOR EACH ROW EXECUTE PROCEDURE update_updated_at();

CREATE TABLE "tenant" (
  id UUID PRIMARY KEY,
  "name" text,
  website text,
  zoneinfo text,
  locale text,
  phone_number text,
  street_address text,
  locality text,
  region text,
  postal_code text,
  country text,
  is_managing boolean default FALSE,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);
CREATE TRIGGER update_tenant_updated_at BEFORE UPDATE ON "tenant" FOR EACH ROW EXECUTE PROCEDURE update_updated_at();

CREATE TABLE "tenant_user" (
  tenant_id UUID not null references "tenant"(id),
  user_id UUID not null references "user"(id),
  role text,

  PRIMARY KEY(tenant_id, user_id)
);

CREATE TABLE application (
  id text PRIMARY KEY,
  tenant_id UUID not null references "tenant"(id),
  "name" text,
  "description" text,
  image text,
  secret_digest text,
  redirect_uri text,
  is_active boolean default TRUE,
  is_system boolean not null default FALSE,
  is_default boolean not null default FALSE,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);
CREATE TRIGGER update_application_updated_at BEFORE UPDATE ON "application" FOR EACH ROW EXECUTE PROCEDURE update_updated_at();

CREATE TABLE "provider" (
  id serial PRIMARY KEY,
  name text UNIQUE,
  website text
);

CREATE TABLE "identity" (
  id serial PRIMARY KEY,
  user_id UUID not null references "user"(id),
  provider_id integer not null references "provider"(id),
  token_type text not null,
  token text,
  created_at timestamp with time zone default now()
);

CREATE TABLE "scope" (
  id SERIAL PRIMARY KEY,
  "scope" text,
  "description" text,
  allow_access text[],
  display boolean default TRUE,
  created_at timestamp with time zone default now()
);
CREATE TRIGGER update_scope_updated_at BEFORE UPDATE ON "scope" FOR EACH ROW EXECUTE PROCEDURE update_updated_at();
CREATE UNIQUE INDEX unique_scope ON "scope" ((lower("scope")));

-- INITIAL DATA
INSERT INTO "scope" ("scope", "description", allow_access, display)
VALUES
('openid', 'login session', '{}', true),
('profile', 'basic profile information', '{"name","family_name","given_name","middle_name","nickname","preferred_username","profile","picture","website","gender","birthdate","zoneinfo","locale","updated_at"}', true),
('email', 'email address', '{"email","email_verified"}', true),
('address', 'physical address', '{"address"}', true),
('phone', 'phone number', '{"phone_number","phone_number_verified"}', true),
('offline_access', 'access your data offline', '{}', true);

INSERT INTO provider(id, name) VALUES (1, 'sesame');
