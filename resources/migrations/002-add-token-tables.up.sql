-- Each application can choose whether to receive a bearer token or some sort
-- of token that conveys information, such as a JWT. By default simple bearer
-- tokens are used.
ALTER TABLE "application" ADD COLUMN "token_type" text not null default 'bearer';

CREATE TABLE access_token (
       token_digest text PRIMARY KEY,
       token_type text not null DEFAULT 'bearer',
       application_id text not null references "application"(id),
       is_revoked boolean not null default FALSE,
       nbf timestamp with time zone default now(),
       exp timestamp with time zone default (now() + interval '1 hour')
);

CREATE TABLE refresh_token (
       token_digest text PRIMARY KEY,
       access_token_digest text not null references access_token (token_digest),
       application_id text not null references "application" (id),
       is_revoked boolean not null default FALSE,
       nbf timestamp with time zone default now(),
       exp timestamp with time zone default (now() + interval '7 days')
);
CREATE INDEX refresh_token_access_token_digest ON refresh_token USING hash (access_token_digest);

CREATE OR REPLACE FUNCTION revoke_refresh_token()
       RETURNS trigger AS
$$
BEGIN
        UPDATE refresh_token
        SET is_revoked = TRUE
        WHERE access_token_digest = NEW.token_digest;

        RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER revoke_refresh_token_on_access_token_revoke
    BEFORE UPDATE OF is_revoked ON access_token
    FOR EACH ROW
    WHEN ( NEW.is_revoked = TRUE )
    EXECUTE PROCEDURE revoke_refresh_token();
